import '../css/main.scss';

// 数据双向绑定
import watch from '../../lib/mc-watch';

// 代码着色插件
import McTinting from '@fekit/mc-tinting';
// 新建一个代码着色实例
new McTinting({
  theme: 'vscode'
});

let data = {
  title: '这是一个标题',
  content: '这是一段内容',
  lists: {
    state: 0,
    color: 'red'
  },
  div1: '这是一个可编辑的DIV'
};

// 实例代码1
watch({
  data,
  item: 'title',
  from: document.getElementById('input'),
  updated(title) {
    console.log(title);
    document.getElementById('title').innerText = title;
  }
});
// 实例代码2
watch({
  data,
  item: 'content',
  from: document.getElementById('textarea'),
  updated(content) {
    console.log(content);
    document.getElementById('content').innerText = content;
  }
});

// 实例代码3
watch({
  data: data.lists,
  item: 'state',
  from: document.getElementById('checkbox'),
  updated(state) {
    console.log(state);
    document.getElementById('lists').setAttribute('data-state', state);
  }
});

// 实例代码4
watch({
  data: data,
  item: 'div1',
  from: document.getElementById('div2'),
  updated(div1) {
    console.log(div1);
    document.getElementById('div1').innerText = div1;
  }
});

let data4 = {
  show: 1
};
watch({
  data: data4,
  item: 'show',
  updated(show) {
    document.getElementById('div4').setAttribute('data-show', show);
    console.log(show);
    console.log(data4.show);
  }
});
document.getElementById('btn6').onclick = function () {
  console.log(data4.show);
  data4.show = (data4.show === 1 ? 2 : 1);
  console.log(data4.show);
};

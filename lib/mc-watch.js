function isDom(obj) {
  return (typeof HTMLElement === 'object') ? (obj instanceof HTMLElement) : !!(obj && typeof obj === 'object' && (obj.nodeType === 1 || obj.nodeType === 9) && typeof obj.nodeName === 'string');
}

/**
 * 数据双向绑定
 * @param {{}}   param           入参为一个对象
 * @param {Object}   param.data      数据监听
 * @param {String}   param.item      数据属性名
 * @param {Element}   param.from     数据来源的输入框
 * @param {Function} param.updated   数据更新回调
 * */
let watch = function (param = {}) {

  if (param && param.data.hasOwnProperty(param.item)) {
    let updated = typeof param.updated === 'function' ? param.updated : function () { };
    let data = param.data || {};
    let item = param.item;
    let from = param.from;
    let last = data[item];
    let event = 'input';
    let type = 'value';
    if (isDom(from)) {
      if (from.localName === 'input' || from.localName === 'textarea') {
        if (from.type === 'checkbox') {
          type = 'checked';
        }
      } else {
        event = 'DOMSubtreeModified';
        type = from.nodeValue ? 'nodeValue' : 'innerText';
      }
    }
    let value = data[item];
    updated(value, last);
    Object.defineProperty(data, item, {
      get: () => {
        return value;
      },
      set: (last) => {
        updated(last, value);
        value = last;
      }
    });

    if (isDom(from) && from[type] !== 'undefined') {
      if (from[type] !== data[item]) {
        from[type] = data[item];
      }
      from.addEventListener(event, (e) => {
        if (event !== 'input') {
          type = e.target.nodeValue ? 'nodeValue' : 'innerText';
        }
        if (data[item] !== e.target[type]) {
          data[item] = e.target[type];
        }
      });
    }

  }
};

export default watch;

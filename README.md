# @FEKIT/MC-WATCH
```$xslt
一个数据双向绑定的小功能插件
```

#### 索引
* [演示](#演示)
* [参数](#参数)
* [示例](#示例)
* [版本](#版本)
* [反馈](#反馈)


#### 演示
[http://fekit.asnowsoft.com/plugins/mc-watch](http://fekit.asnowsoft.com/plugins/mc-watch)


#### 开始

下载项目: 

```npm
npm i @fekit/mc-watch
```

#### 参数
```$xslt
{
  data:                         需要监听数据的对象
  item:                         监听对象中的一个属性名
  from:                         数据修改的来源，一般为一个input标签，但是也可以是其它标签；也可以不填，直接操作数据对象
  updated(newValue,oldValue):   数据变化时的回调,回调可以取得两个参数，一个是更新的值，一个是原来的值
}
```

#### 示例

```javascript
  import watch from '@fekit/mc-watch';

  let data = {
    title:'这是一个标题'
  }

  // 实例代码1
  watch({
    data: data,
    item: 'title',
    from: document.getElementById('input'),
    updated(newValue,oldValue) {
      console.log('原来的数据是' + oldValue);
      console.log('更新的数据是' + newValue);
      document.getElementById('title').innerText = newValue;
    }
  });

```

#### 版本
```$xslt [last vension]
v1.0.1
1. 修复一个更新数据后执行两次的BUG
```

```$xslt
v1.0.0
1. 完成基础功能
```

#### 反馈
```$xslt
如果您在使用中遇到问题，请通过以下方式联系我。
email: xiaojunbo@126.com
```
